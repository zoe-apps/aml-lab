# Copyright (c) 2016, Daniele Venzano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Spark-Jupyter Zoe application description generator."""

import json
import sys
import os

APP_NAME = 'aml-lab-spark'
ZOE_APPLICATION_DESCRIPTION_VERSION = 3

options = {
    'master_hard_mem_limit': {
        'value': int(2.5 * (1024**3)),
        'description': 'Spark Master kill memory limit (bytes)'
    },
    'master_soft_mem_limit': {
        'value': 2 * (1024**3),
        'description': 'Spark Master soft memory limit (bytes)'
    },
    'worker_hard_mem_limit':{
        'value': 10 * (1024**3),
        'description': 'Spark Worker kill memory limit (bytes)'
    },
    'worker_soft_mem_limit':{
        'value': 6 * (1024**3),
        'description': 'Spark Worker soft memory limit (bytes)'
    },
    'notebook_soft_mem_limit': {
        'value': 6 * (1024**3),
        'description': 'Notebook soft memory limit (bytes)'
    },
    'notebook_hard_mem_limit': {
        'value': 8 * (1024**3),
        'description': 'Notebook kill memory limit (bytes)'
    },
    'worker_cores': {
        'value': 1,
        'description': 'Cores used by each worker'
    },
    'worker_count': {
        'value': 4,
        'description': 'Number of workers'
    },
    'hdfs_namenode': {
        'value': 'hdfs-namenode.bigfoot.eurecom.fr',
        'description': 'Namenode hostname'
    }
}

REPOSITORY = os.getenv("REPOSITORY", default="zapps")
VERSION = os.getenv("VERSION", default="latest")

MASTER_IMAGE = REPOSITORY + "/spark2-master-aml:" + VERSION
WORKER_IMAGE = REPOSITORY + "/spark2-worker-aml:" + VERSION
NOTEBOOK_IMAGE = REPOSITORY + "/spark2-jupyter-notebook-aml:" + VERSION
SUBMIT_IMAGE = REPOSITORY + "/spark2-submit-aml:" + VERSION

def spark_master_service(soft_mem_limit, hard_mem_limit):
    """
    :type mem_limit: int
    :type image: str
    :rtype: dict
    """
    service = {
        'name': "spark-master",
        'image': MASTER_IMAGE,
        'monitor': False,
        'resources': {
            "memory": {
                "min": soft_mem_limit,
                "max": hard_mem_limit
            },
            "cores": {
                'min': 0.1,
                'max': 1
            }
        },
        'ports': [
            {
                'name': "Spark master web interface",
                'protocol': "tcp",
                'port_number': 8080,
                'url_template': "http://{ip_port}/",
            }
        ],
        'environment': [
            ["SPARK_MASTER_IP", "{dns_name#self}"],
            ["HADOOP_USER_NAME", "{user_name}"],
            ["PYTHONHASHSEED", "42"],
        ],
        'volumes': [],
        'total_count': 1,
        'essential_count': 1,
        'startup_order': 0,
        'replicas': 1,
        'command': None
    }
    return service


def spark_worker_service(count, soft_mem_limit, hard_mem_limit, cores):
    """
    :type count: int
    :type mem_limit: int
    :type cores: int
    :rtype List(dict)

    :param count: number of workers
    :param mem_limit: hard memory limit for workers
    :param cores: number of cores this worker should use
    :return: a list of service entries
    """
    worker_ram = hard_mem_limit - (1024 ** 3) - (512 * 1024 ** 2)
    service = {
        'name': "spark-worker",
        'image': WORKER_IMAGE,
        'monitor': False,
        'resources': {
            "memory": {
                "min": soft_mem_limit,
                "max": hard_mem_limit
            },
            "cores": {
                'min': 1,
                'max': cores
            }
        },
        'ports': [],
        'environment': [
            ["SPARK_WORKER_CORES", str(cores)],
            ["SPARK_WORKER_RAM", str(worker_ram)],
            ["SPARK_MASTER_IP", "{dns_name#spark-master0}"],
            ["SPARK_LOCAL_IP", "{dns_name#self}"],
            ["PYTHONHASHSEED", "42"],
            ["HADOOP_USER_NAME", "{user_name}"]
        ],
        'volumes': [],
        'total_count': count,
        'essential_count': 1,
        'startup_order': 1,
        'replicas': 1,
        'command': None
    }
    return service


def spark_jupyter_notebook_service(soft_mem_limit, hard_mem_limit, worker_mem_limit, hdfs_namenode):
    """
    :type mem_limit: int
    :type worker_mem_limit: int
    :type hdfs_namenode: str
    :rtype: dict
    """
    executor_ram = worker_mem_limit - (1024 ** 3) - (512 * 1025 ** 2)
    driver_ram = (2 * 1024 ** 3)
    service = {
        'name': "spark-jupyter",
        'image': NOTEBOOK_IMAGE,
        'monitor': True,
        'resources': {
            "memory": {
                "min": soft_mem_limit,
                "max": hard_mem_limit
            },
            "cores": {
                'min': 0.5,
                'max': 2
            }
        },
        'ports': [
            {
                'name': "Jupyter Notebook interface",
                'protocol': "tcp",
                'port_number': 8888,
                'url_template': "http://{ip_port}/",
            }
        ],
        'environment': [
            ["SPARK_MASTER", "spark://{dns_name#spark-master0}:7077"],
            ["SPARK_EXECUTOR_RAM", str(executor_ram)],
            ["SPARK_DRIVER_RAM", str(driver_ram)],
            ["HADOOP_USER_NAME", "{user_name}"],
            ["NB_USER", "{user_name}"],
            ["PYTHONHASHSEED", "42"],
            ['NAMENODE_HOST', hdfs_namenode]
        ],
        'volumes': [],
        'total_count': 1,
        'essential_count': 1,
        'startup_order': 0,
        'replicas': 1,
        'command': None
    }
    return service


if __name__ == '__main__':
    sp_master = spark_master_service(options['master_soft_mem_limit']['value'], options['master_hard_mem_limit']['value'])
    sp_worker = spark_worker_service(options['worker_count']['value'], options['worker_soft_mem_limit']['value'], options['worker_hard_mem_limit']['value'], options['worker_cores']['value'])
    jupyter = spark_jupyter_notebook_service(options['notebook_soft_mem_limit']['value'], options['notebook_hard_mem_limit']['value'], options['worker_hard_mem_limit']['value'], options['hdfs_namenode']['value'])

    app = {
        'name': APP_NAME,
        'version': ZOE_APPLICATION_DESCRIPTION_VERSION,
        'will_end': False,
        'size': 648,
        'services': [
            sp_master,
            sp_worker,
            jupyter
        ]
    }

    json.dump(app, open("aml-lab-zapp.json", "w"), sort_keys=True, indent=4)

    print("ZApp written")

